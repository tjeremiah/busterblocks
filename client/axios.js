const axiosImport = require('axios');

const baseURL = 'https://api.busterblocks.local';

const axios = axiosImport.create({
  baseURL,
  withCredentials: true
});

export { axios };
