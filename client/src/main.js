import Vue from 'vue';
import vuetify from '@/plugins/vuetify';
import VueCsrf from 'vue-csrf';
import router from './router';
import App from './App.vue';
import store from './store';
import './registerServiceWorker';
import VueResource from 'vue-resource';

Vue.config.productionTip = false;

Vue.use(VueCsrf);
Vue.use(VueResource);

new Vue({
  router,
  store,
  vuetify,
  render: (h) => h(App),
}).$mount('#app');
