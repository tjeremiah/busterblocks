export default {
  Success: "Login Successful",
  Fail: "There was an issue logging in, please check your credentials"
}
