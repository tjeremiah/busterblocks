export default {
  Success: "Registration Successful",
  ExistingEmail: "An account already exists for the Email address provided",
  MissingField: "There was an issue with the registration, please ensure you have filled in all fields"
}
