export default {
  Success: "Order Successful! Your order number is: ",
  Fail: "There was an issue with your order, please check you have filled in all required fields and try again"
}
