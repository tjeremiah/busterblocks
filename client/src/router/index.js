import Vue from 'vue';
import VueRouter from 'vue-router';
import Login from '@/views/Login.vue';
import Products from '@/views/Products.vue';
import Cart from '@/views/Cart.vue';
import Checkout from "@/views/Checkout";
import Home from '../views/Home.vue';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
  },
  {
    path: '/dvds',
    name: 'DVDs',
    component: Products,
  },
  {
    path: '/cart',
    name: 'Cart',
    component: Cart,
  },
  {
    path: '/checkout',
    name: 'Cart',
    component: Checkout,
  }
];

const router = new VueRouter({
  mode: 'history',
  routes,
});

export default router;
