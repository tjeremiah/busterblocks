import Vue from 'vue';
import Vuex from 'vuex';
import {axios} from '../../../axios';

Vue.use(Vuex);

export default {
  namespaced: true,
  state: {
    cart: [],
    subCart: [],
    routeToCheckout: false,
  },
  getters: {
    cart: (state) => state.cart,
    routeToCheckout: (state) => state.routeToCheckout,
  },
  mutations: {
    setCart(state, cart) {
      state.cart = cart;
    },
    increaseQuantity(state, id) {
      state.cart.products.forEach(product => {
        if (product.product.id === id) {
          product.qty++
        }
      })
    },
    decreaseQuantity(state, id) {
      state.cart.products.forEach(product => {
        if (product.product.id === id) {
          product.qty--
        }
      })
    },
    setRouteToCheckout(state, status) {
      state.routeToCheckout = status;
    }
  },
  actions: {
    async fetchCart({commit}) {
      const response = await axios.get('/cart');

      if (response.data.length) {
        commit('setCart', JSON.parse(response.data));
      }
    },
    async increaseQuantity({commit, dispatch}, key) {
      commit('increaseQuantity', key);
      dispatch('updateCart');
    },
    async decreaseQuantity({commit, dispatch}, key) {
      commit('decreaseQuantity', key);
      dispatch('updateCart');

    },
    async updateCart({commit, getters}) {
      let cart = [];

      getters.cart.products.forEach(product => {
        let productObj = {
          id: product.product.id,
          qty: product.qty
        }

        cart.push(productObj);
      })

      const response = await axios.put('/cart', {
        cart
      });

      if (response.data.length) {
        commit('setCart', JSON.parse(response.data));
      }
    },
    async addToCart({commit}, [id, qty]) {
      const response = await axios.post('/cart', {
        id, qty
      });

      if (response) {
        commit('setCart', JSON.parse(response.data));
      }
    },
    async clearCart({commit}) {
      let response = await axios.delete('/cart');

      if (response) {
        commit('setCart', [])
      }
    },
    setRouteToCheckout({commit}, status) {
      commit('setRouteToCheckout', status);
    },
  },
};
