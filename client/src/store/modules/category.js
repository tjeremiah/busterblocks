import Vue from 'vue';
import Vuex from 'vuex';
import { axios } from '../../../axios';

Vue.use(Vuex);

export default {
  namespaced: true,
  state: {
    categories: [],
  },
  getters: {
    categories: (state) => state.categories,
  },
  mutations: {
    setCategories(state, categories) {
      state.categories = categories;
    },
  },
  actions: {
    async fetchCategories({ commit }) {
      const response = await axios.get('/categories');

      if (response.status === 200) {
        commit('setCategories', JSON.parse(response.data));
      }
    },
  },
};
