import Vue from 'vue';
import Vuex from 'vuex';
import { axios } from '../../../axios';

Vue.use(Vuex);

export default {
  namespaced: true,
  state: {
    products: [],
  },
  getters: {
    products: (state) => state.products,
  },
  mutations: {
    setProducts(state, products) {
      state.products = products;
    },
  },
  actions: {
    async fetchProducts({ commit }, category) {
      //If a category is defined only load the movies for that category.
      if(category) {
        const response = await axios.get('/products/category/' + category);

        if (response.status === 200) {
          commit('setProducts', JSON.parse(response.data));
        }
      } else {
        const response = await axios.get('/products');

        if (response.status === 200) {
          commit('setProducts', JSON.parse(response.data));
        }
      }
    },
  },
};
