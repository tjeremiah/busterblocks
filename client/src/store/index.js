import Vue from 'vue';
import Vuex from 'vuex';
import product from '@/store/modules/product';
import cart from '@/store/modules/cart';
import category from "@/store/modules/category";
import { axios } from '../../axios';

Vue.use(Vuex);

const HOME = 'home';
const LOGIN = 'login';
const REGISTER = 'register';

export default new Vuex.Store({
  state: {
    loggedIn: false,
    showLogInRegisterModal: false,
    logIn: false,
    register: false,
    home: true,
    email: ''
  },
  getters: {
    loggedIn: (state) => state.loggedIn,
    logInRegisterModal: (state) => state.showLogInRegisterModal,
    logIn: (state) => state.logIn,
    register: (state) => state.register,
    home: (state) => state.home,
    email: (state) => state.email
  },
  mutations: {
    logIn(state) {
      state.loggedIn = true;
    },
    logOut(state) {
      state.loggedIn = false;
    },
    showModal(state) {
      state.showLogInRegisterModal = true;
    },
    hideModal(state) {
      state.showLogInRegisterModal = false;
    },
    setModalLogIn(state) {
      state.register = false;
      state.home = false;
      state.logIn = true;
    },
    setModalRegister(state) {
      state.logIn = false;
      state.home = false;
      state.register = true;
    },
    setModalHome(state) {
      state.logIn = false;
      state.register = false;
      state.home = true;
    },
    setEmail(state, email) {
      state.email = email;
    },
  },
  actions: {
    async setLoggedIn({ commit }) {
      try {
        const response = await axios.get('/authenticated', {
          withCredentials: true,
        });

        if (response && response.status === 200 && response.data) {
          commit('logIn');
          commit('setEmail', response.data);
        } else {
          commit('logOut');
          commit('setEmail', '');
        }
      } catch (e) {
        commit('logOut');
        commit('setEmail', '');
      }
    },
    setModal({commit}, type) {
      if(type === LOGIN) {
        commit('setModalLogIn');
      } else if(type === REGISTER) {
        commit('setModalRegister');
      } else {
        commit('setModalHome');
      }
    },
    openModal({commit}) {
      commit('showModal');
    },
    closeModal({commit}) {
      commit('hideModal');
    },
    setEmail({commit}, email) {
      commit('setEmail', email);
    }
  }
  ,
  modules: {
    product,
    cart,
    category
  },
});
