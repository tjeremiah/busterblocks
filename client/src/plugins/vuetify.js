import Vue from 'vue';
import Vuetify from 'vuetify/lib/framework';
import 'vuetify/dist/vuetify.min.css';

Vue.use(Vuetify);

const opts = {
  theme: {
    themes: {
      light: {
        primary: '#0E3DA4',
        secondary: '#FFFFFF',
        accent: '#F7A403',
        error: '#c72118',
        info: '#F7A403',
        success: '#06d45f',
        warning: '#F7A403',
      },
      dark: {
        primary: '#0E3DA4',
        secondary: '#FFFFFF',
        accent: '#F7A403',
        error: '#c72118',
        info: '#F7A403',
        success: '#06d45f',
        warning: '#F7A403',
      },
    },
  },
};

export default new Vuetify(opts);
