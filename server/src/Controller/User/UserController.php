<?php

namespace App\Controller\User;

use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

//TODO add security to all endpoints
class UserController extends AbstractController
{
    public const REQUIRED_FIELDS = [
        "email",
        "password"
    ];

    public const ALLOWED_FIELDS = [
        "email",
        "password"
    ];

    /**
     * Get all users.
     *
     * This will return all users.
     *
     * @Route("/users", methods={"GET"})
     *
     * @param UserRepository $user_repository
     *
     * @return JsonResponse
     */
    public function getUsers(UserRepository $user_repository): JsonResponse
    {
        //Get all users
        $users = $user_repository->findAll();

        if ($users) {
            return $this->json($users);
        }

        return $this->json($users, Response::HTTP_NOT_FOUND);
    }

    /**
     * Get a user.
     *
     * This will return a single user if it matches the specified ID.
     *
     * @Route("/users/{id}", methods={"GET"})
     *
     * @param int $id
     * @param UserRepository $user_repository
     *
     * @return JsonResponse
     */
    public function getUserById(int $id,
                                UserRepository $user_repository): JsonResponse
    {
        //Get the defined user.
        $user = $user_repository->find($id);

        if ($user) {
            return $this->json($user);
        }

        return $this->json($user, Response::HTTP_NOT_FOUND);
    }

    /**
     * Create a user.
     *
     * This will create a new user using the data supplied.
     *
     * @Route("/users", methods={"POST"})
     *
     * @param Request $request
     * @param UserRepository $user_repository
     * @param EntityManagerInterface $entity_manager
     * @param UserPasswordEncoderInterface $password_encoder
     *
     * @return JsonResponse
     */
    public function createUser(Request $request,
                               UserRepository $user_repository,
                               EntityManagerInterface $entity_manager,
                               UserPasswordEncoderInterface $password_encoder): JsonResponse
    {
        $request_data = json_decode($request->getContent());

        //Do not proceed of any of the required fields are missing.
        foreach (self::REQUIRED_FIELDS as $field) {
            if (!property_exists($request_data, $field)) {
                return $this->json(null, Response::HTTP_UNPROCESSABLE_ENTITY);
            }
        }

        //Make sure required fields are not blank.
        foreach ($request_data as $key => $data) {
            foreach (self::REQUIRED_FIELDS as $field) {
                if ($key == $field && $data == "") {
                    return $this->json(null, Response::HTTP_UNPROCESSABLE_ENTITY);
                }
            }
        }

        $user = new User();

        foreach ($request_data as $field => $data) {
            if (in_array($field, self::ALLOWED_FIELDS)) {
                if ($field == "email") {
                    //Check if an account exists with the email address.
                    if ($user_repository->findBy(['email' => $data])) {
                        return $this->json(null, Response::HTTP_CONFLICT);
                    }
                    $user->setEmail($data);
                }

                if ($field == "password") {
                    //Check this meets the password requirements.
                    if(strlen($data) < 8 || strlen($data) > 50 || !preg_match('~[0-9]~', $data) > 0) {
                        return $this->json(null, Response::HTTP_UNPROCESSABLE_ENTITY);
                    }
                    $user->setPassword($password_encoder->encodePassword($user, $data));
                }
            }
        }

        $entity_manager->persist($user);
        $entity_manager->flush();

        return $this->json($user->getEmail(), Response::HTTP_CREATED);
    }

    /**
     * Delete a user.
     *
     * This will delete a single product if it matches the specified ID.
     *
     * @Route("/users/{id}", methods={"DELETE"})
     *
     * @param int $id
     * @param UserRepository $user_repository
     * @param EntityManagerInterface $entity_manager
     *
     * @return JsonResponse
     */
    public function deleteUser(int $id,
                               UserRepository $user_repository,
                               EntityManagerInterface $entity_manager): JsonResponse
    {
        //Make sure the user exists.
        $user = $user_repository->find($id);

        if ($user) {
            //Remove the user.
            $entity_manager->remove($user);
            $entity_manager->flush();

            return $this->json(null, Response::HTTP_NO_CONTENT);
        }

        return $this->json($user, Response::HTTP_NOT_FOUND);
    }
}