<?php


namespace App\Controller\User;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;

class UserAuthenticationController extends AbstractController
{
    /**
     * @Route("/authenticated")
     *
     * @return JsonResponse
     */
    public function isAuthenticated(SessionInterface $session):JsonResponse
    {
        //Load the user.
        $user = $this->getUser();

        if($user) {
            //Return if the user has the correct role.
            if(in_array('ROLE_USER', $user->getRoles())) {
                return $this->json($user->getUsername());
            }
        }

        return $this->json(null);
    }
}