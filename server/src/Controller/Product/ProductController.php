<?php

namespace App\Controller\Product;

use App\Entity\Category;
use App\Entity\Product;
use App\Repository\CategoryRepository;
use App\Repository\ProductRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Constraints\Json;

//TODO add security to all endpoints
class ProductController extends AbstractController
{
    public const REQUIRED_FIELDS = [
        "name",
        "description",
        "category"
    ];

    public const ALLOWED_FIELDS = [
        "name",
        "description",
        "category",
        "image",
        "price",
        "stock_quantity",
        "visibility"
    ];

    /**
     * Get all products.
     *
     * This will return all products.
     *
     * @Route("/products", methods={"GET"})
     *
     * @param ProductRepository $product_repository
     * @param SerializerInterface $serializer
     *
     * @return JsonResponse
     */
    public function getProducts(ProductRepository $product_repository,
                                SerializerInterface $serializer): JsonResponse
    {
        //Get all products
        $products = $product_repository->findAll();

        if ($products) {
            //Serialize the products.
            //I would normally use the $this->json() method, however I needed to add serializer groups
            //to fix a circular reference with the category.
            $products_json = $serializer->serialize(
                $products,
                'json',
                ['groups' => ['products']]);

            return new JsonResponse($products_json);
//            return $this->json($products);
        }

        return $this->json($products, Response::HTTP_NOT_FOUND);
    }

    /**
     * Get products by their category.
     *
     * This will load all products with the specified category.
     *
     * @Route("/products/category/{id}", methods={"GET"})
     *
     * @param int $id
     * @param CategoryRepository $category_repository
     * @param SerializerInterface $serializer
     *
     * @return JsonResponse
     */
    public function getProductsByCategory(int $id,
                                          CategoryRepository $category_repository,
                                          SerializerInterface $serializer): JsonResponse
    {
        //Load the category.
        $category = $category_repository->find($id);

        if($category) {
            //Load all of the products for the category.
            $products = $category->getProducts();

            if ($products) {
                //Serialize the products.
                //I would normally use the $this->json() method, however I needed to add serializer groups
                //to fix a circular reference with the category.
                $products_json = $serializer->serialize(
                    $products,
                    'json',
                    ['groups' => ['products']]);

                return new JsonResponse($products_json);
//            return $this->json($products);
            }
        }

        return $this->json(null, Response::HTTP_NOT_FOUND);
    }

    /**
     * Get a product.
     *
     * This will return a single product if it matches the specified ID.
     *
     * @Route("/products/{id}", methods={"GET"})
     *
     * @param int $id
     * @param ProductRepository $product_repository
     * @param SerializerInterface $serializer
     *
     * @return JsonResponse
     */
    public function getProduct(int $id,
                               ProductRepository $product_repository,
                               SerializerInterface $serializer): JsonResponse
    {
        //Get the defined product.
        $product = $product_repository->find($id);

        if ($product) {
            //Serialize the product.
            //I would normally use the $this->json() method, however I needed to add serializer groups
            //to fix a circular reference with the category.
            $product_json = $serializer->serialize($product,
                'json',
                ['groups' => ['products']]);

            return new JsonResponse($product_json);
//            return $this->json($product);
        }

        return $this->json($product, Response::HTTP_NOT_FOUND);
    }

    /**
     * Create a product.
     *
     * This will create a new product using the data supplied.
     *
     * @Route("/products", methods={"POST"})
     *
     * @param Request $request
     * @param EntityManagerInterface $entity_manager
     * @param SerializerInterface $serializer
     *
     * @return JsonResponse
     */
    public function createProduct(Request $request,
                                  EntityManagerInterface $entity_manager,
                                  SerializerInterface $serializer): JsonResponse
    {
        //TODO Created 201 response
        //Decode the request content.
        $request_data = json_decode($request->getContent());

        //Do not proceed of any of the required fields are missing.
        foreach (self::REQUIRED_FIELDS as $field) {
            if (!property_exists($request_data, $field)) {
                return $this->json(null, Response::HTTP_UNPROCESSABLE_ENTITY);
            }
        }

        $product = new Product();

        //Loop over all fields and set their values.
        foreach ($request_data as $field => $data) {
            if ($field == "category") {
                //Get the category.
                $data = $entity_manager->getRepository(Category::class)->find($data);
            }

            //Only update allowed fields.
            if (in_array($field, self::ALLOWED_FIELDS)) {
                //Construct the necessary function name for this field.
                $function_name = "set" . ucfirst($field);

                $product->{$function_name}($data);
            }
        }

        $entity_manager->persist($product);
        $entity_manager->flush();

        $product_json = $serializer->serialize(
            $product,
            'json',
            ['groups' => ['products']]);

        return new JsonResponse($product_json);

        //TODO Handle what happens if the field data is not the correct type.
        //TODO Handle how uploading images will work.
    }

    /**
     * Replace a product.
     *
     * This will replace a products fields with the data supplied.
     * Data for all of the required fields must be sent for this function to execute.
     * If only a subset of the required fields are to be updated, the PATCH endpoint should be used instead.
     *
     * @Route("/products/{id}", methods="PUT")
     *
     * @param int $id
     * @param Request $request
     * @param EntityManagerInterface $entity_manager
     * @param ProductRepository $product_repository
     * @param SerializerInterface $serializer
     *
     * @return JsonResponse
     */
    public function replaceProduct(int $id,
                                   Request $request,
                                   EntityManagerInterface $entity_manager,
                                   ProductRepository $product_repository,
                                   SerializerInterface $serializer): JsonResponse
    {
        $product = $product_repository->find($id);

        if ($product) {
            //Decode the request content.
            $request_data = json_decode($request->getContent());

            //Do not proceed of any of the required fields are missing.
            foreach (self::REQUIRED_FIELDS as $field) {
                if (!property_exists($request_data, $field)) {
                    return $this->json(null, Response::HTTP_UNPROCESSABLE_ENTITY);
                }
            }

            //Loop over all fields and set their values.
            foreach ($request_data as $field => $data) {
                if ($field == "category") {
                    //Get the category.
                    $data = $entity_manager->getRepository(Category::class)->find($data);
                }

                //Only update allowed fields.
                if (in_array($field, self::ALLOWED_FIELDS)) {
                    //Construct the necessary function name for this field.
                    $function_name = "set" . ucfirst($field);

                    $product->{$function_name}($data);
                }
            }

            $entity_manager->flush();

            $product_json = $serializer->serialize(
                $product,
                'json',
                ['groups' => ['products']]);

            return new JsonResponse($product_json);
        }

        return $this->json($product, Response::HTTP_NOT_FOUND);

        //TODO Handle what happens if the field data is not the correct type.
        //TODO Handle how uploading images will work.
    }

    /**
     * Update a product.
     *
     * This will update a products fields with the data supplied.
     * This does not require data for all of the required fields and can update a subset of the fields.
     * If all of the fields are to be updated, the PUT endpoint would be better suited.
     *
     * @Route("/products/{id}", methods={"PATCH"})
     *
     * @param int $id
     * @param Request $request
     * @param EntityManagerInterface $entity_manager
     * @param ProductRepository $product_repository
     * @param SerializerInterface $serializer
     *
     * @return JsonResponse
     */
    public function updateProduct(int $id,
                                  Request $request,
                                  EntityManagerInterface $entity_manager,
                                  ProductRepository $product_repository,
                                  SerializerInterface $serializer): JsonResponse
    {
        $product = $product_repository->find($id);

        if ($product) {
            //Decode the request content.
            $request_data = json_decode($request->getContent());

            //Loop over all fields and set their values.
            foreach ($request_data as $field => $data) {
                if ($field == "category") {
                    //Get the category.
                    $data = $entity_manager->getRepository(Category::class)->find($data);
                }

                //Only update allowed fields.
                if (in_array($field, self::ALLOWED_FIELDS)) {
                    //Construct the necessary function name for this field.
                    $function_name = "set" . ucfirst($field);

                    $product->{$function_name}($data);
                }
            }

            $entity_manager->flush();

            $product_json = $serializer->serialize(
                $product,
                'json',
                ['groups' => ['products']]);

            return new JsonResponse($product_json);
        }

        return $this->json($product, Response::HTTP_NOT_FOUND);

        //TODO Handle what happens if the field data is not the correct type.
        //TODO Handle how uploading images will work.
    }

    /**
     * Delete a product.
     *
     * This will delete a single product if it matches the specified ID.
     *
     * @Route("/products/{id}", methods={"DELETE"})
     *
     * @param int $id
     * @param ProductRepository $product_repository
     * @param EntityManagerInterface $entity_manager
     *
     * @return JsonResponse
     */
    public function deleteProduct(int $id,
                                  ProductRepository $product_repository,
                                  EntityManagerInterface $entity_manager): JsonResponse
    {
        //Make sure the product exists.
        $product = $product_repository->find($id);

        if ($product) {
            //Remove the product.
            $entity_manager->remove($product);
            $entity_manager->flush();

            return $this->json(null, Response::HTTP_NO_CONTENT);
        }

        return $this->json($product, Response::HTTP_NOT_FOUND);
    }
}