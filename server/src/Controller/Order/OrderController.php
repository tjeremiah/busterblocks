<?php

namespace App\Controller\Order;

use App\Repository\OrderRepository;
use App\Service\Cart\CartService;
use App\Service\Order\OrderService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class OrderController extends AbstractController
{
    public const REQUIRED_FIELDS = [
        "door",
        "street",
        "postcode"
    ];

    /**
     * Place an order.
     *
     * This will create an order and add all of the products in the cart to that order.
     *
     * @Route("/order", methods={"POST"})
     *
     * @param Request $request
     * @param CartService $cart_service
     * @param OrderService $order_service
     *
     * @return JsonResponse
     */
    public function placeOrder(Request $request,
                               CartService $cart_service,
                               OrderService $order_service): JsonResponse
    {
        //Deny access unless the user accessing this controller is fully authenticated.
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        //Decode the order details.
        $order_details = json_decode($request->getContent());

        //Do not proceed of any of the required fields are missing.
        foreach (self::REQUIRED_FIELDS as $field) {
            if (!property_exists($order_details, $field)) {
                return $this->json(null, Response::HTTP_UNPROCESSABLE_ENTITY);
            }
        }

        //Make sure required fields are not blank.
        foreach ($order_details as $key => $data) {
            foreach (self::REQUIRED_FIELDS as $field) {
                if ($key == $field && $data == "") {
                    return $this->json(null, Response::HTTP_UNPROCESSABLE_ENTITY);
                }
            }
        }

        //TODO check required fields.

        //Load the user.
        $user = $this->getUser();

        if($user) {
            //Fetch the cart.
            $cart = $cart_service->fetchCart();

            if(!empty($cart)) {
                //Create the order.
                $order = $order_service->createOrder($order_details, $user);

                if($order) {
                    //Add the cart products to the order.
                    $order_service->addProductsToOrder($order, $cart);

                    return $this->json($order->getId(), Response::HTTP_CREATED);
                }
            }
        }
        return $this->json(null);
    }
}