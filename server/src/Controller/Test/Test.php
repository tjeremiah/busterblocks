<?php

namespace App\Controller\Test;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;

class Test extends AbstractController
{
    /**
     * @Route("/test1", methods={"POST"})
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function test1(Request $request)
    {
        $request_data = json_decode($request->getContent());

        dump($request_data);

        return new JsonResponse([
            'success' => true
        ]);
    }

    /**
     * @Route("/test2", methods={"POST"})
     *
     * @param Request $request
     * @param SessionInterface $session
     * @return JsonResponse
     */
    public function test2(Request $request,
                          SessionInterface $session): JsonResponse
    {
        dump($session);

        $session->set('test', 1);

        dump($session);

        return $this->json(null);
    }
}