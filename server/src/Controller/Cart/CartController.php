<?php

namespace App\Controller\Cart;

use App\Service\Cart\CartService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class CartController extends AbstractController
{
    /**
     * Add a product to the cart.
     *
     * This will add the item to the cart and return the cart to the client.
     *
     * @Route("/cart", methods={"POST"})
     *
     * @param Request $request
     * @param CartService $cart_service
     * @param SerializerInterface $serializer
     *
     * @return JsonResponse
     */
    public function addToCart(Request $request,
                              CartService $cart_service,
                              SerializerInterface $serializer): JsonResponse
    {
        $request_data = json_decode($request->getContent());

        //Add the item to the cart.
        $cart = $cart_service->addToCart($request_data);

        if (!empty($cart)) {
            //Structure cart with entities and quantities.
            $constructed_cart = $cart_service->constructCart($cart);

            //Serialize the constructed cart.
            $cart_json = $serializer->serialize($constructed_cart, 'json', ['groups' => 'products']);

            return new JsonResponse($cart_json);
        }

        //Return an empty cart.
        return $this->json([]);
    }

    /**
     * Update the cart.
     *
     * This receives an entire updated cart from the client and updates the cart.
     * The purpose of this is if the user has updated the cart from the cart screen and it needs to be recalculated.
     *
     * @Route("/cart", methods={"PUT"})
     *
     * @param Request $request
     * @param CartService $cart_service
     * @param SerializerInterface $serializer
     *
     * @return JsonResponse
     */
    public function updateCart(Request $request,
                               CartService $cart_service,
                               SerializerInterface $serializer): JsonResponse
    {
        $cart_data = json_decode($request->getContent());

        //Update the cart.
        $cart = $cart_service->updateCart($cart_data->cart);

        if (!empty($cart)) {
            //Structure cart with entities and quantities.
            $constructed_cart = $cart_service->constructCart($cart);

            //Serialize the constructed cart.
            $cart_json = $serializer->serialize($constructed_cart, 'json', ['groups' => 'products']);

            return new JsonResponse($cart_json);
        }

        //Return an empty cart.
        return $this->json([]);
    }

    /**
     * Get the cart.
     *
     * This will load the cart from the session.
     * It will then load the products in the cart, calculate the costs and return this to the client.
     *
     * @Route("/cart", methods={"GET"})
     *
     * @param CartService $cart_service
     * @param SerializerInterface $serializer
     *
     * @return JsonResponse
     */
    public function fetchCart(CartService $cart_service,
                              SerializerInterface $serializer): JsonResponse
    {
        //Fetch cart
        $cart = $cart_service->fetchCart();

        if (!empty($cart)) {
            //Structure cart with entities and quantities.
            $constructed_cart = $cart_service->constructCart($cart);

            //Serialize the constructed cart.
            $cart_json = $serializer->serialize($constructed_cart, 'json', ['groups' => 'products']);

            return new JsonResponse($cart_json);
        }

        return $this->json([]);
    }

    /**
     * Clear the cart.
     *
     * This will clear the cart from the session and return an empty cart to update the front end.
     *
     * @Route("/cart", methods={"DELETE"})
     *
     * @param SessionInterface $session
     *
     * @return JsonResponse
     */
    public function clearCart(SessionInterface $session):JsonResponse
    {
        $session->set('cart', []);

        return $this->json([], Response::HTTP_NO_CONTENT);
    }
}