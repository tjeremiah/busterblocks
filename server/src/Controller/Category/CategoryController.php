<?php

namespace App\Controller\Category;

use App\Entity\Category;
use App\Repository\CategoryRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Routing\Annotation\Route;

//TODO add security to all endpoints
class CategoryController extends AbstractController
{
    public const REQUIRED_FIELDS = [
        "name"
    ];

    public const ALLOWED_FIELDS = [
        "name"
    ];

    /**
     * Get all categories
     *
     * This will return all categories.
     *
     * @Route("/categories", methods={"GET"})
     *
     * @param CategoryRepository $category_repository
     * @param SerializerInterface $serializer
     *
     * @return JsonResponse
     */
    public function getCategories(CategoryRepository $category_repository,
                                  SerializerInterface $serializer): JsonResponse
    {
        //Get all categories
        $categories = $category_repository->findAll();

        if ($categories) {
            //Serialize the categories.
            //I would normally use the $this->json() method, however I needed to add serializer groups
            //to fix a circular reference with the products.
            $categories_json = $serializer->serialize(
                $categories,
                'json',
                ['groups' => ['categories']]);

            return new JsonResponse($categories_json);
        }

        return $this->json($categories, Response::HTTP_NOT_FOUND);
    }

    /**
     * Get a category
     *
     * This will return a single category if it matches the specified ID.
     *
     * @Route("/categories/{id}", methods={"GET"})
     *
     * @param int $id
     * @param CategoryRepository $category_repository
     * @param SerializerInterface $serializer
     *
     * @return JsonResponse
     */
    public function getCategory(int $id,
                                CategoryRepository $category_repository,
                                SerializerInterface $serializer): JsonResponse
    {
        // Get the defined product.
        $category = $category_repository->find($id);

        if ($category) {
            //Serialize the category.
            //I would normally use the $this->json() method, however I needed to add serializer groups
            //to fix a circular reference with the products.
            $category_json = $serializer->serialize($category,
                'json',
                ['groups' => ['categories']]);

            return new JsonResponse($category_json);
        }

        return $this->json($category, Response::HTTP_NOT_FOUND);
    }

    /**
     * Create a category.
     *
     * This will create a new category using the data supplied.
     *
     * @Route("/categories", methods={"POST"})
     *
     * @param Request $request
     * @param EntityManagerInterface $entity_manager
     * @param SerializerInterface $serializer
     *
     * @return JsonResponse
     */
    public function createCategory(Request $request,
                                   EntityManagerInterface $entity_manager,
                                   SerializerInterface $serializer): JsonResponse
    {
        //TODO Created 201 response
        $request_data = json_decode($request->getContent());

        //Do not proceed of any of the required fields are missing.
        foreach (self::REQUIRED_FIELDS as $field) {
            if (!property_exists($request_data, $field)) {
                return $this->json(null, Response::HTTP_UNPROCESSABLE_ENTITY);
            }
        }

        $category = new Category();

        //Loop over all fields and set their values.
        foreach ($request_data as $field => $data) {
            //Only update allowed fields.
            if (in_array($field, self::ALLOWED_FIELDS)) {
                //Construct the necessary function name for this field.
                $function_name = "set" . ucfirst($field);

                $category->{$function_name}($data);
            }
        }

        $entity_manager->persist($category);
        $entity_manager->flush();

        $category_json = $serializer->serialize(
            $category,
            'json',
            ['groups' => ['categories']]);

        return new JsonResponse($category_json);

        //TODO Handle what happens if the field data is not the correct type.
        //TODO Handle how uploading images will work.
    }

    /**
     * Delete a category.
     *
     * This will delete a single category if it matches the specified ID.
     *
     * @Route("/categories/{id}", methods={"DELETE"})
     *
     * @param int $id
     * @param CategoryRepository $category_repository
     * @param EntityManagerInterface $entity_manager
     *
     * @return JsonResponse
     */
    public function deleteProduct(int $id,
                                  CategoryRepository $category_repository,
                                  EntityManagerInterface $entity_manager): JsonResponse
    {
        //TODO what happens to the product if its category is deleted?
        //Make sure the category exists.
        $category = $category_repository->find($id);

        if($category) {
            //Remove the category.
            $entity_manager->remove($category);
            $entity_manager->flush();

            return $this->json(null, Response::HTTP_NO_CONTENT);
        }

        return $this->json($category, Response::HTTP_NOT_FOUND);
    }
}