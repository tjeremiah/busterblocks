<?php

namespace App\Entity\Traits;

use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * Trait TimestampTrait
 * @package App\Entity\Traits
 */
trait TimestampTrait
{
    /**
     * @ORM\Column(type="datetime")
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updated_at;

    /** @ORM\PrePersist() */
    public function doOnPrePersist()
    {
        dump(new DateTime('now'));
        $this->created_at = new DateTime('now');
        $this->updated_at = new DateTime('now');
        dump($this->created_at);
        dump($this->updated_at);
    }

    /** @ORM\PreUpdate() */
    public function doOnPreUpdate()
    {
        $this->updated_at = new DateTime('now');
    }
}