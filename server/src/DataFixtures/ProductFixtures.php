<?php


namespace App\DataFixtures;


use App\Entity\Product;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class ProductFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $film1 = new Product();
        $film1->setName('Die hard');
        $film1->setDescription('Blah blah blah');
        $film1->setCategory($this->getReference('category_action'));
        $film1->setImage('imageroute');
        $film1->setPrice(7.00);
        $film1->setStockQuantity(5);

        $film2 = new Product();
        $film2->setName('Jumping Jack Flash');
        $film2->setDescription('Blah');
        $film2->setCategory($this->getReference('category_comedy'));
        $film2->setImage('imageroute');
        $film2->setPrice(4.00);
        $film2->setStockQuantity(10);

        $film3 = new Product();
        $film3->setName('The Notebook');
        $film3->setDescription('Blah Blah');
        $film3->setCategory($this->getReference('category_romance'));
        $film3->setImage('imageroute');
        $film3->setPrice(6.50);
        $film3->setStockQuantity(7);

        $manager->persist($film1);
        $manager->persist($film2);
        $manager->persist($film3);

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            CategoryFixtures::class
        ];
    }
}