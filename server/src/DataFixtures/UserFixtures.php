<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends Fixture
{
    private $password_encoder;

    public function __construct(UserPasswordEncoderInterface $password_encoder) {
        $this->password_encoder = $password_encoder;
    }

    public function load(ObjectManager $manager)
    {
        $user_test_1 = new User();
        $user_test_1->setEmail('test1@test.com');
        $user_test_1->setPassword($this->password_encoder->encodePassword($user_test_1, 'testpassword'));

        $manager->persist($user_test_1);

        $manager->flush();
    }
}
