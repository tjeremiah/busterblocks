<?php


namespace App\DataFixtures;


use App\Entity\Category;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class CategoryFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $category_action = new Category();
        $category_action->setName('Action');

        $category_comedy = new Category();
        $category_comedy->setName('Comedy');

        $category_romance = new Category();
        $category_romance->setName('Romance');

        $manager->persist($category_action);
        $manager->persist($category_comedy);
        $manager->persist($category_romance);

        $manager->flush();

        $this->addReference('category_action', $category_action);
        $this->addReference('category_comedy', $category_comedy);
        $this->addReference('category_romance', $category_romance);
    }
}