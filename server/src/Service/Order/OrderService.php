<?php

namespace App\Service\Order;

use App\Entity\Order;
use App\Entity\ProductOrder;
use App\Repository\ProductRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class OrderService
{
    private EntityManagerInterface $entity_manager;
    private ProductRepository $product_repository;

    public function __construct(EntityManagerInterface $entity_manager,
                                ProductRepository $product_repository)
    {
        $this->entity_manager = $entity_manager;
        $this->product_repository = $product_repository;
    }

    /**
     * Create an order.
     *
     * This will creat an order and return the order.
     *
     * @param object $order_details
     * @param UserInterface $user
     *
     * @return Order
     */
    public function createOrder(object $order_details, UserInterface $user): Order
    {
        $order = new Order();
        $order->setUser($user);
        $order->setDoor($order_details->door);
        $order->setStreet($order_details->street);
        $order->setPostcode($order_details->postcode);

        $this->entity_manager->persist($order);
        $this->entity_manager->flush();

        return $order;
    }

    /**
     * Add products to an order.
     *
     * This will add products to an order.
     *
     * @param Order $order
     * @param array $cart
     *
     * @return Order
     */
    public function addProductsToOrder(Order $order, array $cart): Order
    {
        foreach ($cart as $cart_item) {
            $product = $this->product_repository->find($cart_item['id']);
            if($product) {
                $product_order = new ProductOrder();
                $product_order->setTheOrder($order);
                $product_order->setProduct($product);
                $product_order->setQuantity($cart_item['qty']);

                $this->entity_manager->persist($product_order);
            }
        }

        $this->entity_manager->flush();

        return $order;
    }
}