<?php

//@ini_set("session.cookie_secure", "Off");

namespace App\Service\Cart;

use App\Repository\ProductRepository;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class CartService
{
    protected SessionInterface $session;
    protected ProductRepository $product_repository;

    public function __construct(SessionInterface $session,
                                ProductRepository $product_repository)
    {
        $this->session = $session;
        $this->product_repository = $product_repository;
    }

    /**
     * Add a product to the cart.
     *
     * This will add a product to the cart or update the quantity if the product is already in the cart.
     *
     * @param $data
     * @return array
     */
    public function addToCart($data): array
    {
        $cart = [];
        $exists = false;

        //Check if a cart already exists.
        if (!empty($this->session->get('cart'))) {
            $cart = $this->session->get('cart');
        }

        foreach ($cart as $key => $product) {
            //Check if this product exists in the cart already.
            if($product["id"] == $data->id) {
                $exists = true;

                //Update the products quantity.
                $cart[$key]["qty"] = $product["qty"] + $data->qty;
            }
        }

        //Add the product to the cart if it does not already exist.
        if(!$exists) {
            $data_array = [
                "id" => $data->id,
                "qty" => $data->qty
            ];

            array_push($cart, $data_array);
        }

        //Add or update the cart in the session.
        $this->session->set('cart', $cart);

        return $cart;
    }

    /**
     * Update the cart.
     *
     * This will update the cart .
     * This entirely replaces the cart as the client is sending the entire cart after updating it on the front end.
     *
     * @param $cart_data
     * @return array
     */
    public function updateCart($cart_data): array
    {
        //Blank out the cart.
        $cart = [];

        //Loop over each product supplied and add them to the cart.
        foreach ($cart_data as $data) {
            //Construct the array for each cart entry.
            $data_array = [
                "id" => $data->id,
                "qty" => $data->qty
            ];

            array_push($cart, $data_array);
        }

        //Set the session cart.
        $this->session->set('cart', $cart);

        return $cart;
    }

    /**
     * Fetch the cart.
     * This will load the cart from the session.
     *
     * @return array
     */
    public function fetchCart(): array
    {
        //Pull the cart from the session.
        $cart = $this->session->get('cart');

        if (!empty($cart)) {
            return $cart;
        }

        return [];
    }

    /**
     * Construct the cart.
     *
     * This will take the cart array and construct the cart for the front end.
     * This includes loading in the product, calculating the products total cost and calculating the carts total cost.
     *
     * @param array $cart
     * @return array
     */
    public function constructCart(array $cart): array
    {
        //Create a blank cart.
        $cart_array = [
            "products" => [],
            "cartCost" => 0
        ];

        //Loop over each entry in the cart.
        foreach ($cart as $cart_entry) {
            //Load the product.
            $product = $this->product_repository->find($cart_entry["id"]);

            //Check the product exists.
            if ($product) {
                //Calculate the total cost for this product.
                $product_cost = $cart_entry["qty"] * $product->getPrice();

                $product_array = [
                    "product" => $product,
                    "qty" => $cart_entry["qty"],
                    "productCost" => $product_cost
                ];

                //Add the product to the cart array.
                array_push($cart_array["products"], $product_array);

                //Add this cost to the total cart cost.
                $cart_array["cartCost"] += $product_cost;
            }
        }

        return $cart_array;
    }
}